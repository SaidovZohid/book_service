package main

import (
	"log"
	"net"

	"book/book_service/service"
	"book/book_service/storage"

	pb "book/book_service/genproto/book_service"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	psqlUrl := "host=postgres port=5432 user=postgres password=1234 dbname=books_db sslmode=disable"

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect: %v", err)
	}

	strg := storage.NewBook(psqlConn)

	se := service.New(strg)

	listen, err := net.Listen("tcp", ":5000")

	s := grpc.NewServer()
	pb.RegisterBookServiceServer(s, se)
	reflection.Register(s)

	log.Println("gRPC server started port in: :5000")

	if s.Serve(listen);err != nil {
		log.Fatalf("Error while listening: %v", err)
	}
}
