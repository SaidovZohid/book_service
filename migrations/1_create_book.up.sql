create table if not exists "books" (
    "id" serial primary key,
    "title"  varchar(200) not null,
    "author"  varchar(60) not  null,
    "description"  text not null,
    "language" varchar(50)  not null
);