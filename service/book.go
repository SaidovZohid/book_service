package service

import (
	pb "book/book_service/genproto/book_service"
	"book/book_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BookService struct {
	pb.UnimplementedBookServiceServer
	strg *storage.BookRepo
}

func New(strg *storage.BookRepo) *BookService {
	return &BookService{
		strg: strg,
	}
}

func (bs *BookService) Create(ctx context.Context, req *pb.Book) (*pb.Book, error) {
	book, err := bs.strg.CreateBook(&storage.Book{
		Id:          req.Id,
		Title:       req.Title,
		Author:      req.Author,
		Description: req.Description,
		Language:    req.Language,
	})

	if err != nil {
		return nil, status.Errorf(codes.Internal, "internatl server error: %v", err)
	}

	return &pb.Book{
		Id:          book.Id,
		Title:       book.Title,
		Author:      book.Author,
		Description: book.Description,
		Language:    book.Language,
	}, nil
}

func (bs *BookService) Get(ctx context.Context, req *pb.GetBookRequest) (*pb.Book, error) {
	book, err := bs.strg.GetBook(req.Id)

	if err != nil {
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &pb.Book{
		Id:          book.Id,
		Title:       book.Title,
		Author:      book.Author,
		Description: book.Description,
		Language:    book.Language,
	}, nil
}
