package storage

import "github.com/jmoiron/sqlx"

type BookRepo struct {
	db *sqlx.DB
}

func NewBook(db *sqlx.DB) *BookRepo {
	return &BookRepo{db: db}
}

type Book struct {
	Id          int64
	Title       string
	Author      string
	Description string
	Language    string
}

func (d *BookRepo) CreateBook(b *Book) (*Book, error) {
	query := `
		INSERT INTO books (
			title,
			author,
			description,
			language
		) VALUES ($1, $2, $3, $4)
		RETURNING id
	`
	err := d.db.QueryRow(
		query,
		b.Title,
		b.Author,
		b.Description,
		b.Language,
	).Scan(&b.Id)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (d *BookRepo) GetBook(bookId int64) (*Book, error) {
	var book Book
	query := `
		SELECT 
			id,
			title,
			author,
			description,
			language
		FROM books WHERE id = $1
	`
	err := d.db.QueryRow(
		query,
		bookId,
	).Scan(
		&book.Id,
		&book.Title,
		&book.Author,
		&book.Description,
		&book.Language,
	)
	if err != nil {
		return nil, err
	}

	return &book, nil
}
